from django.urls import path
from . import views
from .views import MatkulView, MatkulDetail, MatkulDelete
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('profile', views.profile, name='profile'),
    path('games', views.games, name='games'),
    path('matkul', MatkulView.as_view(), name='matkul'),
    path('detail/<int:pk>/', MatkulDetail.as_view(), name='detail-matkul'),
    path('input-matkul', views.input_matkul, name='input-matkul'),
    path('detail/<int:pk>/remove', MatkulDelete.as_view(), name='remove-matkul'),
]
