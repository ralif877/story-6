from django import forms
from .models import Matkul

class MatkulForm(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = [
            'matakuliah',
            'dosen',
            'sks',
            'deskripsi',
            'semester',
            'kelas'
        ]
        widgets = {
            'matakuliah':forms.TextInput(attrs={'class':'form-control','placeholder':'Masukkan Nama Matakuliah'}),
            'dosen':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Masukkan Nama Dosen'}),
            'sks':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Masukkan Jumlah SKS'}),
            'deskripsi':forms.Textarea(attrs={'class':'form-control', 'placeholder':'Masukkan Deskripsi Matakuliah'}),
            'semester':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Masukkan Semester, Contoh: Gasal 2021/2020'}),
            'kelas':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Masukkan Ruang Kelas, Contoh: R2.4001'})
        }

