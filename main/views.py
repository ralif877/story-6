from django.shortcuts import render
from .forms import MatkulForm
from .models import Matkul
from django.views.generic import ListView, DetailView, DeleteView
from django.urls import reverse_lazy

def home(request):
    return render(request, 'main/home.html')

def profile(request):
    return render(request, 'main/profile.html')

def games(request):
    return render(request, 'main/games.html')

def input_matkul(request):
    form = MatkulForm(request.POST or None)
    if form.is_valid():
        form.save()

    context = {
        "form":form
    }
    return render(request, 'main/input-matkul.html', context)

class MatkulView(ListView):
    model = Matkul
    template_name = "main/matkul.html"

class MatkulDetail(DetailView):
    model = Matkul
    template_name = "main/detail-matkul.html"

class MatkulDelete(DeleteView):
    model = Matkul
    template_name = "main/remove-matkul.html"
    success_url = reverse_lazy('main:matkul')
