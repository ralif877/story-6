from django.urls import path
from . import views

app_name = 'Kegiatan'

urlpatterns = [
    path('', views.kegiatan, name='kegiatan'),
    path('/tambah-peserta', views.tambah_peserta, name='tambah-peserta')
]
