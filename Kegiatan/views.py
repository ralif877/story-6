from django.shortcuts import render
from .forms import PesertaForm
from .models import Peserta
from django.views.generic import ListView, DetailView, DeleteView
from django.urls import reverse_lazy
# Create your views here.
def kegiatan(request):
    return render(request, 'main/kegiatan.html')

def tambah_peserta(request):
    form = PesertaForm(request.POST or None)
    if form.is_valid():
        form.save()

    context = {
        "form":form
    }
    return render(request, 'main/tambah-peserta.html', context)
