from django.db import models

# Create your models here.
class Peserta(models.Model):
    pilihan = (
            ('DOTA2','Mabar DOTA 2'),
            ('ML', 'Mabar ML'),
            ('LOL', 'Mabar LOL')
    )
    nama = models.CharField(max_length=300)
    kegiatan = models.CharField(max_length=300, choices=pilihan)
    
    def __str__(self):
        return self.nama