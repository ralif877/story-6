from django import forms
from .models import Peserta

class PesertaForm(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = [
            'nama',
            'kegiatan'
        ]
        
        widgets = {
            'nama':forms.TextInput(attrs={'class':'form-control'}),
            'kegiatan':forms.ChoiceField(label="", initial='', widget=forms.Select(), required=True)
        }